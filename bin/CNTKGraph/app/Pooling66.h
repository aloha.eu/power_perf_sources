// File automatically generated by ESPAM

#ifndef Pooling66_H
#define Pooling66_H
#include "csdfNode.h"
#include <map>

class Pooling66 : public csdfNode{
public:
    Pooling66();
    virtual ~Pooling66();

    void main(void *threadarg) override;
    // specific const parameters definition
    std::map<std::string,int> int_params;
  const int neurons = 8;

  //FIFO sizes
    int IP0_fifo_size;
    int OP0_fifo_size;

  const int input_dims = 3;
  //input array definition
    const int input_dim_0 = 8;
    const int input_dim_1 = 2;
    const int input_dim_2 = 28;
    int input[8][2][28] = {{{0}}};

  const int output_dims = 3;
  //output array definition
    const int output_dim_0 = 8;
    const int output_dim_1 = 1;
    const int output_dim_2 = 14;
    int output[8][1][14] = {{{0}}};

//const parameters
  const int k_h = 2;
  const int k_w = 2;
  const int stride = 2;
  //specific node parameters and functions
};
#endif // Pooling66_H
