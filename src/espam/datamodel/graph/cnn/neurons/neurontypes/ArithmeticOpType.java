package espam.datamodel.graph.cnn.neurons.neurontypes;

/**
 * Describes concrete type of arithmetic operation
 */
public enum ArithmeticOpType {
    MATMUL,ADD, GEMM, MUL
}
