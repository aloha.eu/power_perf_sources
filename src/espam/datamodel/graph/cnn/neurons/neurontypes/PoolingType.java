package espam.datamodel.graph.cnn.neurons.neurontypes;

/**
 * Describes concrete types of Pooling neuron
 */
public enum PoolingType {
    MAXPOOL,AVGPOOL,GLOBALAVGPOOL,GLOBALLPPOOL,GLOBALMAXPOOL,LPPOOL
}
