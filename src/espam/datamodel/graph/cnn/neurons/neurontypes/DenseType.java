package espam.datamodel.graph.cnn.neurons.neurontypes;

/**
 * Describes the concrete type of the NonLinear Neuron
 */
public enum DenseType {
    MATMUL, GEMM, DENSEBLOCK
}
