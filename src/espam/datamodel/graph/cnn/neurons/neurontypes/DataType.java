package espam.datamodel.graph.cnn.neurons.neurontypes;

/**
 * Describes concrete type of Data neuron
 */
public enum DataType {
    INPUT,OUTPUT,CONST
}
