
package espam.visitor.xps.cdpn;

//////////////////////////////////////////////////////////////////////////
//// 

/**
* 
*
* @author  Teddy Zhai
* @version  $Id: Options.java,v 1.1 2012/05/02 16:25:45 tzhai Exp $
*/

public class Options {
    // use local variables for self channels
    public static boolean USE_LOCAL_VAR_FIFO = true;
    public static boolean USE_FULLY_AUTOMATED_SDK = false;
}
