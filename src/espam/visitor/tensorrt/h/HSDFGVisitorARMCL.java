package espam.visitor.tensorrt.h;

import espam.datamodel.graph.cnn.Layer;
import espam.datamodel.graph.cnn.Network;
import espam.utils.fileworker.FileWorker;
import espam.visitor.CNNGraphVisitor;

public class HSDFGVisitorARMCL extends CNNGraphVisitor {

        /**
     * Call .h visitor
     * @param dnn DNN
     * @param dir target directory for templates
     */
     public void callDNNVisitor(Network dnn, String dir){
         String className = dnn.getName();
         try {

             _printStream = FileWorker.openFile(dir, className, "h");
             _writeCommonBeginning(dnn,"Example");

             /** write specific data*/
             _printStream.println(_prefix + "//specific node parameters and functions");
             _writeCommonEnd(className);
         }
         catch (Exception e){
             System.err.println(".h file creation error for " + className + ". " + e.getMessage());
         }
     }

     /**
     * Begin a header file with common beginning
     * @param  dnn DNN
     */
    public void _writeCommonBeginning(Network dnn, String baseClassName ) {

         String name = dnn.getName();
        _printStream.println("// File automatically generated by ESPAM");
        _printStream.println("");
        _printStream.println("#ifndef " + name + "_H");
        _printStream.println("#define " + name + "_H");
        _printStream.println("");
        //ARM CL classes
        _printStream.println("#include \"arm_compute/graph.h\"");
        _printStream.println("#include \"support/ToolchainSupport.h\"");
        _printStream.println("#include \"utils/CommonGraphOptions.h\"");
        _printStream.println("#include \"utils/GraphUtils.h\"");
        _printStream.println("#include \"utils/Utils.h\"");

        _printStream.println("#include <chrono>");
        _printStream.println("#include <thread>");

        _printStream.println("");
        _printStream.println(_prefix + "using namespace arm_compute::utils;");
        _printStream.println(_prefix + "using namespace arm_compute::graph::frontend;");
        _printStream.println(_prefix + "using namespace arm_compute::graph_utils;");
        _printStream.println("");
        _printStream.println("class " + name + " : public " + baseClassName + " {");
        _printStream.println("public:");
        _prefixInc();
        _printStream.println(_prefix + name + "(): cmd_parser(), common_opts(cmd_parser), common_params(), graph(0, \"" + name + "\") { };");

        _printStream.println("");

        _printStream.println(_prefix + "//DNN-dependent functions");
        _printStream.println(_prefix + "bool do_setup(int argc, char **argv) override;// DNN");
        _printStream.println(_prefix + "void do_run() override; //run DNN");

        _printStream.println();
        _printStream.println(_prefix + "//DNN-dependent parameters");
        _initParams(dnn);
        _printStream.println();

         _printStream.println("private:");
         _printStream.println(_prefix + "CommandLineParser  cmd_parser;");
         _printStream.println(_prefix + "CommonGraphOptions common_opts;");
         _printStream.println(_prefix + "CommonGraphParams  common_params;");
         _printStream.println(_prefix + "Stream             graph;");
         _printStream.println();
        _prefixDec();
    }
    
     /**
     * Write init parameters function
     *
     * @param dnn DNN
     */
    protected void _initParams(Network dnn) {

    Layer inputLayer = dnn.getInputLayer();
    Layer outputLayer = dnn.getOutputLayer();


    _printStream.println(_prefix +" int batchSize = 1;");
    _printStream.println(_prefix +" const char* INPUT_BLOB_NAME = \"" + inputLayer.getName() + "\";");
    _printStream.println(_prefix +" const char* OUTPUT_BLOB_NAME = \"" + outputLayer.getName() + "\";");

    _printStream.println(_prefix +" int INPUT_C = " + inputLayer.getInputChannels() + ";");
    _printStream.println(_prefix +" int INPUT_H = " + inputLayer.getInpH()+ ";");
    _printStream.println(_prefix +" int INPUT_W = " + inputLayer.getInpW() + ";");
    _printStream.println();

    _printStream.println(_prefix +" int OUTPUT_C = " + outputLayer.getOutputChannels() + ";");
    _printStream.println(_prefix +" int OUTPUT_H = " + outputLayer.getOutpH() + ";");
    _printStream.println(_prefix +" int OUTPUT_W = " + outputLayer.getOutpW() + ";");
    _printStream.println(_prefix +" int OUTPUT_SIZE = OUTPUT_C * OUTPUT_H * OUTPUT_W;");

    }

    /**
     * Finish a header file with common ending
     * @param  className name of the corresponding C++ class
     */
    public void _writeCommonEnd(String className) {
        _printStream.println("};");
        _printStream.println("#endif // " + className + "_H");
    }


}
