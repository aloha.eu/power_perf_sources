/*******************************************************************\
  * 
  The ESPAM Software Tool 
  Copyright (c) 2004-2011 Leiden University (LERC group at LIACS).
  All rights reserved.
  
  The use and distribution terms for this software are covered by the 
  Common Public License 1.0 (http://opensource.org/licenses/cpl1.0.txt)
  which can be found in the file LICENSE at the root of this distribution.
  By using this software in any fashion, you are agreeing to be bound by 
  the terms of this license.
  
  You must not remove this notice, or any other, from this software.
  
  \*******************************************************************/

package espam.visitor.systemc.timed;

//////////////////////////////////////////////////////////////////////////
//// 

/**
 * 
 *
 * @author  Teddy Zhai
 * @version  $Id: RuntimeConfig.java,v 1.2 2011/03/28 15:33:12 tzhai Exp $
 */

public class RuntimeConfig {
    public static boolean IS_DEBUG_MODE = false;
}
