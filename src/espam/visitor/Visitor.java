
package espam.visitor;

//////////////////////////////////////////////////////////////////////////
//// Visitor

/**
 *  This class is a marker Class, to indicate that derived classes are all
 *  visitor.
 *
 * @author  Todor Stefanov, Hristo Nikolov
 * @version  $Id: Visitor.java,v 1.1 2007/12/07 22:07:24 stefanov Exp $
 */

public interface Visitor {
}
