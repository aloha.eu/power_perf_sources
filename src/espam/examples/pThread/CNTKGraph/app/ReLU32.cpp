// File automatically generated by ESPAM

#include "ReLU32.h"
#include <stdlib.h>
#include <iostream>
#include "csdfNode.h"
#include "appMain.h"
#include "appFunc.h"
#include "fifo.h"
#include <cstddef>
#include "types.h"
#include <vector>
#include <string>
using namespace std;

ReLU32::ReLU32() : csdfNode() {
  //assign FIFO sizes
  IP0_fifo_size = 224;
  OP0_fifo_size = 6300;
  //OP0_fifo_size = 226;

  int_params["input_dims"] = input_dims;
  int_params["input_dim_0"] = input_dim_0;
  int_params["output_dims"] = output_dims;
  int_params["output_dim_0"] = output_dim_0;
}
ReLU32::~ReLU32() {}

void ReLU32::main(void *threadarg) {
  // create communication channel
  thread_info *thread_data;
  thread_data = (struct thread_info *) threadarg;
  fifo_buf* ReLU32_IP0_buf_ptr = thread_data->get_fifo_buf_by_dst("ReLU32_IP0");
 
  fifo_buf* ReLU32_OP0_buf_ptr = thread_data->get_fifo_buf_by_src("ReLU32_OP0");
 
 
  setaffinity(thread_data->core_id);
  // repetition parameters definition
  int q = 28;
  int phase_len = 28;
  int phase; 
 
  // while (1) {
    // loop over the repetitions number
    for (int rep = 0; rep < q ; rep ++) {
      phase = rep % phase_len;

      //reading
      //max tokens port IP0
      int IP0_tokens = 224;
 
      // readSWF_CPU to input
      readSWF_CPU(ReLU32_IP0_buf_ptr->fifo, &input[0], IP0_tokens, ReLU32_IP0_buf_ptr->fifo_size);

      //execution
      //execution parameters
        for (int n = 0; n < 8; n++) {
        appFunc::execute(std::string("ReLU"),&input[0], NULL, &output[n], &int_params);
      }

      //writing
      //max tokens port OP0
      int OP0_tokens = 224;
 
      // writeSWF_CPU to output
      writeSWF_CPU(ReLU32_OP0_buf_ptr->fifo, &output[0], OP0_tokens, ReLU32_OP0_buf_ptr->fifo_size);
    }// loop over the phases
  cout<<" ReLU32 finished! "<<endl;
  //} while (1)
} // main
