// File automatically generated by ESPAM

#include "Pooling160.h"
#include <stdlib.h>
#include <iostream>
#include "csdfNode.h"
#include "appMain.h"
#include "appFunc.h"
#include "fifo.h"
#include <cstddef>
#include "types.h"
#include <vector>
#include <string>
using namespace std;

Pooling160::Pooling160() : csdfNode() {
  //assign FIFO sizes
  IP0_fifo_size = 672;
  OP0_fifo_size = 64;
//  OP0_fifo_size = 10000;

//const int parameters
  int_params["neurons"] = neurons;
  int_params["k_h"] = k_h;
  int_params["k_w"] = k_w;
  int_params["stride"] = stride;
  int_params["input_dims"] = input_dims;
  int_params["input_dim_0"] = input_dim_0;
  int_params["input_dim_1"] = input_dim_1;
  int_params["input_dim_2"] = input_dim_2;
  int_params["output_dims"] = output_dims;
  int_params["output_dim_0"] = output_dim_0;
  int_params["output_dim_1"] = output_dim_1;
  int_params["output_dim_2"] = output_dim_2;
}
Pooling160::~Pooling160() {}

void Pooling160::main(void *threadarg) {
  // create communication channel
  thread_info *thread_data;
  thread_data = (struct thread_info *) threadarg;
  fifo_buf* Pooling160_IP0_buf_ptr = thread_data->get_fifo_buf_by_dst("Pooling160_IP0");
 
  fifo_buf* Pooling160_OP0_buf_ptr = thread_data->get_fifo_buf_by_src("Pooling160_OP0");
 
 
  setaffinity(thread_data->core_id);
  // repetition parameters definition
  int q = 5;
  int phase_len = 5;
  int phase; 
 
  // while (1) {
    // loop over the repetitions number
    for (int rep = 0; rep < q ; rep ++) {
      phase = rep % phase_len;

      //reading
      //max tokens port IP0
      int IP0_tokens = 672;
      if (phase >= 4)
        IP0_tokens = 448; 
 
      // readSWF_CPU to input
      readSWF_CPU(Pooling160_IP0_buf_ptr->fifo, &input[0][0][0], IP0_tokens, Pooling160_IP0_buf_ptr->fifo_size);

      //execution
      //execution parameters
        for (int n = 0; n < 16; n++) {
        appFunc::execute(std::string("MAXPOOL(3_3_16)"),&input[0][0][0], NULL, &output[n][0][0], &int_params);
      }

      //writing
      //max tokens port OP0
      int OP0_tokens = 0;
      if (phase >= 1)
        OP0_tokens = 64; 
 
      // writeSWF_CPU to output
      if(OP0_tokens>0)
    	  writeSWF_CPU(Pooling160_OP0_buf_ptr->fifo, &output[0][0][0], OP0_tokens, Pooling160_OP0_buf_ptr->fifo_size);
    }// loop over the phases
  cout<<" Pooling160 finished! "<<endl;
  //} while (1)
} // main
