// File automatically generated by ESPAM

#include "Convolution110.h"
#include <stdlib.h>
#include <iostream>
#include "csdfNode.h"
#include "appMain.h"
#include "appFunc.h"
#include "fifo.h"
#include <cstddef>
#include "types.h"
#include <vector>
#include <string>
using namespace std;

Convolution110::Convolution110() : csdfNode() {
  //assign FIFO sizes
  IP0_fifo_size = 560;
//  OP1_fifo_size = 3360;
  OP1_fifo_size = 224;

//const int parameters
  int_params["neurons"] = neurons;
  int_params["k_h"] = k_h;
  int_params["k_w"] = k_w;
  int_params["stride"] = stride;
  int_params["input_dims"] = input_dims;
  int_params["input_dim_0"] = input_dim_0;
  int_params["input_dim_1"] = input_dim_1;
  int_params["input_dim_2"] = input_dim_2;
  int_params["output_dims"] = output_dims;
  int_params["output_dim_0"] = output_dim_0;
  int_params["output_dim_1"] = output_dim_1;
  int_params["output_dim_2"] = output_dim_2;

// fill weights with dummy values 
  for (int w0=0;w0< weights_dim_0;w0++) { 
    for (int w1=0;w1< weights_dim_1;w1++) { 
      for (int w2=0;w2< weights_dim_2;w2++) { 
        for (int w3=0;w3< weights_dim_3;w3++) { 
          weights[w0][w1][w2][w3] = 1; 
          }
        }
      }
    }
}
Convolution110::~Convolution110() {}

void Convolution110::main(void *threadarg) {
  // create communication channel
  thread_info *thread_data;
  thread_data = (struct thread_info *) threadarg;
  fifo_buf* Convolution110_IP0_buf_ptr = thread_data->get_fifo_buf_by_dst("Convolution110_IP0");
 
  fifo_buf* Convolution110_OP1_buf_ptr = thread_data->get_fifo_buf_by_src("Convolution110_OP1");
 
 
  setaffinity(thread_data->core_id);
  // repetition parameters definition
  int q = 14;
  int phase_len = 14;
  int phase; 
 
  // while (1) {
    // loop over the repetitions number
    for (int rep = 0; rep < q ; rep ++) {
      phase = rep % phase_len;

      //reading
 
      // internal shift of input
 //     appFunc::shift_3D(input_dim_0,input_dim_1,input_dim_2,&input[0][0][0], stride);
      // shift IP0
      int IP0_shift = 0;
      if (phase >= 1)
        IP0_shift = 448; 
      if (phase >= 10)
        IP0_shift = 0; 
      //max tokens port IP0
      int IP0_tokens = 560;
      if (phase >= 1)
        IP0_tokens = 112; 
      if (phase >= 10)
        IP0_tokens = 0; 
 
      // readSWF_CPU to input
      if(IP0_tokens>0)
    	  readSWF_CPU(Convolution110_IP0_buf_ptr->fifo, &input[0][0][0] + IP0_shift*sizeof(int), IP0_tokens, Convolution110_IP0_buf_ptr->fifo_size);

      //execution
      //execution parameters
        for (int n = 0; n < 16; n++) {
        appFunc::execute(std::string("CONV(5_5_8)"));
      }
    //  cout<<"C110_exec"<<endl;

      //writing
      //max tokens port OP1
      int OP1_tokens = 224;
 
      // writeSWF_CPU to output
      writeSWF_CPU(Convolution110_OP1_buf_ptr->fifo, &output[0][0][0], OP1_tokens, Convolution110_OP1_buf_ptr->fifo_size);
    }// loop over the phases
  cout<<" Convolution110 finished! "<<endl;
  //} while (1)
} // main
