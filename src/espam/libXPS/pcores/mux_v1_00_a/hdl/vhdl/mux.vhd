-- ***********************************************************************
--
-- The ESPAM Software Tool 
-- Copyright (c) 2004-2008 Leiden University (LERC group at LIACS).
-- All rights reserved.
--
-- The use and distribution terms for this software are covered by the 
-- Common Public License 1.0 (http://opensource.org/licenses/cpl1.0.txt)
-- which can be found in the file LICENSE at the root of this distribution.
-- By using this software in any fashion, you are agreeing to be bound by 
-- the terms of this license.
--
-- You must not remove this notice, or any other, from this software.
--
-- ************************************************************************

-- $Id: mux.vhd,v 1.1 2007/12/07 22:08:32 stefanov Exp $

-- mux.vhd
--   Generated by wzhong

library ieee;

use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
-- entity
-------------------------------------------------------------------------------

entity mux is
  generic
  (
    N_MUX  : integer                   := 1
  );
  port
  (
    H_DW0  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from the host
    H_TRI0 : in STD_LOGIC_VECTOR (31 downto 0);  -- tristate from the host 
    H_AD0  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from host 
    H_CO0  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from host
    D_DW0  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from our design
    D_TRI0 : in STD_LOGIC_VECTOR (31 downto 0);  -- tristate from our design
    D_AD0  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from design 
    D_CO0  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from design
    DW0    : out STD_LOGIC_VECTOR (31 downto 0);	-- data write to buffer (sram)
    TRI0   : out STD_LOGIC_VECTOR (31 downto 0); -- tristate to buffer 
    ra0    : out std_logic_vector (19 downto 0);
    rc0    : out std_logic_vector (8 downto 0);
		
		
    H_DW1  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from the host
    H_TRI1 : in STD_LOGIC_VECTOR (31 downto 0);  -- tristate from the host
    H_AD1  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from host 
    H_CO1  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from host
    D_DW1  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from our design
    D_TRI1 : in STD_LOGIC_VECTOR (31 downto 0);  -- tristate from our design
    D_AD1  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from design 
    D_CO1  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from design
    DW1    : out STD_LOGIC_VECTOR (31 downto 0);	-- data write to buffer (sram)
    TRI1   : out STD_LOGIC_VECTOR (31 downto 0); -- tristate to buffer 
    ra1    : out std_logic_vector (19 downto 0);
    rc1    : out std_logic_vector (8 downto 0);
		

    H_DW2  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from the host
    H_TRI2 : in STD_LOGIC_VECTOR (31 downto 0);  -- tristate from the host
    H_AD2  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from host 
    H_CO2  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from host
    D_DW2  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from our design
    D_TRI2 : in STD_LOGIC_VECTOR (31 downto 0);	-- tristate from our design
    D_AD2  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from design 
    D_CO2  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from design
    DW2    : out STD_LOGIC_VECTOR (31 downto 0);	-- data write to buffer (sram)
    TRI2   : out STD_LOGIC_VECTOR (31 downto 0); -- tristate to buffer 
    ra2    : out std_logic_vector (19 downto 0);
    rc2    : out std_logic_vector (8 downto 0);
		
    H_DW3  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from the host
    H_TRI3 : in STD_LOGIC_VECTOR (31 downto 0);  -- tristate from the host
    H_AD3  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from host 
    H_CO3  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from host
    D_DW3  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from our design
    D_TRI3 : in STD_LOGIC_VECTOR (31 downto 0);	-- tristate from our design
    D_AD3  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from design 
    D_CO3  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from design
    DW3    : out STD_LOGIC_VECTOR (31 downto 0);	-- data write to buffer (sram)
    TRI3   : out STD_LOGIC_VECTOR (31 downto 0); -- tristate to buffer 
    ra3    : out std_logic_vector (19 downto 0);
    rc3    : out std_logic_vector (8 downto 0);
		
    H_DW4  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from the host
    H_TRI4 : in STD_LOGIC_VECTOR (31 downto 0);  -- tristate from the host
    H_AD4  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from host 
    H_CO4  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from host
    D_DW4  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from our design
    D_TRI4 : in STD_LOGIC_VECTOR (31 downto 0);	-- tristate from our design
    D_AD4  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from design 
    D_CO4  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from design
    DW4    : out STD_LOGIC_VECTOR (31 downto 0);	-- data write to buffer (sram)
    TRI4   : out STD_LOGIC_VECTOR (31 downto 0); -- tristate to buffer 
    ra4    : out std_logic_vector (19 downto 0);
    rc4    : out std_logic_vector (8 downto 0);
	   
    H_DW5  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from the host
    H_TRI5 : in STD_LOGIC_VECTOR (31 downto 0);  -- tristate from the host
    H_AD5  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from host 
    H_CO5  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from host
    D_DW5  : in STD_LOGIC_VECTOR (31 downto 0);  -- date write from our design
    D_TRI5 : in STD_LOGIC_VECTOR (31 downto 0);	-- tristate from our design
    D_AD5  : in STD_LOGIC_VECTOR (19 downto 0);  -- address from design 
    D_CO5  : in STD_LOGIC_VECTOR (8 downto 0);   -- control from design
    DW5    : out STD_LOGIC_VECTOR (31 downto 0);	-- data write to buffer (sram)
    TRI5   : out STD_LOGIC_VECTOR (31 downto 0); -- tristate to buffer 
    ra5    : out std_logic_vector (19 downto 0);
    rc5    : out std_logic_vector (8 downto 0);
		
    RST   : in std_logic;
    CNTRL : in STD_LOGIC_VECTOR(31 downto 0)
  );
end entity mux;

-------------------------------------------------------------------------------
-- architecture
-------------------------------------------------------------------------------

architecture imp of mux is

  component mux_core is
    generic
    (
      N_MUX  : integer                   := 1
    );
    port
    (
      H_DW  : in STD_LOGIC_VECTOR (191 downto 0);  -- date write from the host
      H_TRI : in STD_LOGIC_VECTOR (191 downto 0);  -- tristate from the host 
      H_AD  : in STD_LOGIC_VECTOR (119 downto 0);  -- address from host 
      H_CO  : in STD_LOGIC_VECTOR (53 downto 0);   -- control from host
      D_DW  : in STD_LOGIC_VECTOR (191 downto 0);  -- date write from our design
      D_TRI : in STD_LOGIC_VECTOR (191 downto 0);  -- tristate from our design
      D_AD  : in STD_LOGIC_VECTOR (119 downto 0);  -- address from design 
      D_CO  : in STD_LOGIC_VECTOR (53 downto 0);   -- control from design
      DW    : out STD_LOGIC_VECTOR (191 downto 0);	-- data write to buffer (sram)
      TRI   : out STD_LOGIC_VECTOR (191 downto 0); -- tristate to buffer 
      ra    : out std_logic_vector (119 downto 0);
      rc    : out std_logic_vector (53 downto 0);
		
      RST   : in std_logic;
      CNTRL : in STD_LOGIC_VECTOR(31 downto 0)
    );
  end component mux_core;
  
  signal sl_H_DW  : std_logic_vector (191 downto 0);
  signal sl_H_TRI : std_logic_vector (191 downto 0);
  signal sl_H_AD  : std_logic_vector (119 downto 0); 
  signal sl_H_CO  : std_logic_vector (53 downto 0);
  signal sl_D_DW  : std_logic_vector (191 downto 0);
  signal sl_D_TRI : std_logic_vector (191 downto 0);
  signal sl_D_AD  : std_logic_vector (119 downto 0); 
  signal sl_D_CO  : std_logic_vector (53 downto 0);
  signal sl_DW    : std_logic_vector (191 downto 0);
  signal sl_TRI   : std_logic_vector (191 downto 0); 
  signal sl_ra    : std_logic_vector (119 downto 0);
  signal sl_rc    : std_logic_vector (53 downto 0);

begin  ------------------------------------------------------------------------
  
  sl_H_DW(31 downto 0) <= H_DW0;
  sl_H_DW(63 downto 32) <= H_DW1;
  sl_H_DW(95 downto 64) <= H_DW2;
  sl_H_DW(127 downto 96) <= H_DW3;
  sl_H_DW(159 downto 128) <= H_DW4;
  sl_H_DW(191 downto 160) <= H_DW5;
  
  sl_H_TRI(31 downto 0) <= H_TRI0;
  sl_H_TRI(63 downto 32) <= H_TRI1;
  sl_H_TRI(95 downto 64) <= H_TRI2;
  sl_H_TRI(127 downto 96) <= H_TRI3;
  sl_H_TRI(159 downto 128) <= H_TRI4;
  sl_H_TRI(191 downto 160) <= H_TRI5;
  
  sl_H_AD(19 downto 0) <= H_AD0;
  sl_H_AD(39 downto 20) <= H_AD1;
  sl_H_AD(59 downto 40) <= H_AD2;
  sl_H_AD(79 downto 60) <= H_AD3;
  sl_H_AD(99 downto 80) <= H_AD4;
  sl_H_AD(119 downto 100) <= H_AD5;
  
  sl_H_CO(8 downto 0) <= H_CO0;
  sl_H_CO(17 downto 9) <= H_CO1;
  sl_H_CO(26 downto 18) <= H_CO2;
  sl_H_CO(35 downto 27) <= H_CO3;
  sl_H_CO(44 downto 36) <= H_CO4;
  sl_H_CO(53 downto 45) <= H_CO5;
  
  sl_D_DW(31 downto 0) <= D_DW0;
  sl_D_DW(63 downto 32) <= D_DW1;
  sl_D_DW(95 downto 64) <= D_DW2;
  sl_D_DW(127 downto 96) <= D_DW3;
  sl_D_DW(159 downto 128) <= D_DW4;
  sl_D_DW(191 downto 160) <= D_DW5;
  
  sl_D_TRI(31 downto 0) <= D_TRI0;
  sl_D_TRI(63 downto 32) <= D_TRI1;
  sl_D_TRI(95 downto 64) <= D_TRI2;
  sl_D_TRI(127 downto 96) <= D_TRI3;
  sl_D_TRI(159 downto 128) <= D_TRI4;
  sl_D_TRI(191 downto 160) <= D_TRI5;
  
  sl_D_AD(19 downto 0) <= D_AD0;
  sl_D_AD(39 downto 20) <= D_AD1;
  sl_D_AD(59 downto 40) <= D_AD2;
  sl_D_AD(79 downto 60) <= D_AD3;
  sl_D_AD(99 downto 80) <= D_AD4;
  sl_D_AD(119 downto 100) <= D_AD5;
  
  sl_D_CO(8 downto 0) <= D_CO0;
  sl_D_CO(17 downto 9) <= D_CO1;
  sl_D_CO(26 downto 18) <= D_CO2;
  sl_D_CO(35 downto 27) <= D_CO3;
  sl_D_CO(44 downto 36) <= D_CO4;
  sl_D_CO(53 downto 45) <= D_CO5;
  
  DW0 <= sl_DW(31 downto 0);
  DW1 <= sl_DW(63 downto 32);
  DW2 <= sl_DW(95 downto 64);
  DW3 <= sl_DW(127 downto 96);
  DW4 <= sl_DW(159 downto 128);
  DW5 <= sl_DW(191 downto 160);
  
  TRI0 <= sl_TRI(31 downto 0);
  TRI1 <= sl_TRI(63 downto 32);
  TRI2 <= sl_TRI(95 downto 64);
  TRI3 <= sl_TRI(127 downto 96);
  TRI4 <= sl_TRI(159 downto 128);
  TRI5 <= sl_TRI(191 downto 160);
  
  ra0 <= sl_ra(19 downto 0);
  ra1 <= sl_ra(39 downto 20);
  ra2 <= sl_ra(59 downto 40);
  ra3 <= sl_ra(79 downto 60);
  ra4 <= sl_ra(99 downto 80);
  ra5 <= sl_ra(119 downto 100);
  
  rc0 <= sl_rc(8 downto 0);
  rc1 <= sl_rc(17 downto 9);
  rc2 <= sl_rc(26 downto 18);
  rc3 <= sl_rc(35 downto 27);
  rc4 <= sl_rc(44 downto 36);
  rc5 <= sl_rc(53 downto 45);
	
	
	
  MUX_CORE_I : mux_core
    generic map
    (
      N_MUX      =>  N_MUX
    )
    port map
    (
      H_DW       =>  sl_H_DW,
      H_TRI      =>  sl_H_TRI,
	  H_AD       =>  sl_H_AD,
	  H_CO       =>  sl_H_CO,
	  D_DW       =>  sl_D_DW,
	  D_TRI      =>	 sl_D_TRI,
	  D_AD       =>  sl_D_AD,
	  D_CO       =>  sl_D_CO,
	  DW         =>  sl_DW,
	  TRI        =>  sl_TRI,
	  ra         =>  sl_ra,
	  rc         =>  sl_rc,
      
	  RST        =>  RST,
	  CNTRL      =>  CNTRL
    );

end architecture imp;

